const _ = require("lodash")
//recibe una lista con numeros pasados por el parseNumber
function filasToStringLn(listFila) {
    return listFila.join("") + "\n"
}

function filasToString(listFilas) {
    return listFilas.map(fila => filasToStringLn(fila)).join('')
}

function printListParsedNumber(list) {
    const filas = []
    for (let i = 0; i < 3; i++) {
        filas[i] = _.flatten(list.map(e => e[i]))
    }
    return filasToString(filas)
}

function printParsedNumber(listNumber) {
    return filasToString(listNumber)
}
module.exports = {
    printListParsedNumber,
    printParsedNumber
}