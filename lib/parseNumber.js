function parseNumber(num) {
    switch (num) {
        case 0:
            return [
                [" ", "_", " "],
                ["|", " ", "|"],
                ["|", "_", "|"]
            ]
        case 1:
            return [
                [" ", " ", " "],
                [" ", " ", "|"],
                [" ", " ", "|"]
            ]
        case 2:
            return [
                [" ", "_", " "],
                [" ", "_", "|"],
                ["|", "_", " "]
            ]
        case 3:
            return [
                [" ", "_", " "],
                [" ", "_", "|"],
                [" ", "_", "|"]
            ]
        case 4:
            return [
                [" ", " ", " "],
                ["|", "_", "|"],
                [" ", " ", "|"]
            ]
        case 5:
            return [
                [" ", "_", " "],
                ["|", "_", " "],
                [" ", "_", "|"]
            ]
        case 6:
            return [
                [" ", "_", " "],
                ["|", "_", " "],
                ["|", "_", "|"]
            ]
        case 7:
            return [
                [" ", "_", " "],
                [" ", " ", "|"],
                [" ", " ", "|"]
            ]
        case 8:
            return [
                [" ", "_", " "],
                ["|", "_", "|"],
                ["|", "_", "|"]
            ]
        case 9:
            return [
                [" ", "_", " "],
                ["|", "_", "|"],
                [" ", "_", "|"]
            ]

        default:
            throw new Error("que me mandaste loco? solo numeros...")
            break;
    }
}

function toListOfIntegers(num) {
    stringNumber = num.toString()
    result = []
    for (let index = 0; index < stringNumber.length; index++) {
        result[index] = parseInt(stringNumber[index])
    }
    return result
}

function parseNumbers(nums) {
    return toListOfIntegers(nums).map(n => parseNumber(n))
}

module.exports = {
    parseNumber,
    parseNumbers
}