 cualquier número (de cualquier cantidad de dígitos) al formato:

       _  _     _  _  _  _  _  _
     | _| _||_||_ |_   ||_||_|| |
     ||_  _|  | _||_|  ||_| _||_|

Siempre siguiendo la práctica de TDD.

La solución a entregar debe ser un repositorio en Gitlab que provea la funcionalidad indicada, y que los tests se corran ante cada commit