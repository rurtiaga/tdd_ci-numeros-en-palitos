const {
    parseNumber,
    parseNumbers
} = require("../lib/parseNumber")
const {
    printParsedNumber,
    printListParsedNumber
} = require("../lib/printParsedNumber")

describe("One character numbers to list of sticks", () => {
    test("it should parse number 0 to list", () => {
        expect(parseNumber(0)).toEqual([
            [" ", "_", " "],
            ["|", " ", "|"],
            ["|", "_", "|"]
        ])
    });
    test("it should parse number 1 to list", () => {
        expect(parseNumber(1)).toEqual([
            [" ", " ", " "],
            [" ", " ", "|"],
            [" ", " ", "|"]
        ])
    });
    test("it should parse number 2 to list", () => {
        expect(parseNumber(2)).toEqual([
            [" ", "_", " "],
            [" ", "_", "|"],
            ["|", "_", " "]
        ])
    });
    test("it should parse number 3 to list", () => {
        expect(parseNumber(3)).toEqual([
            [" ", "_", " "],
            [" ", "_", "|"],
            [" ", "_", "|"]
        ])
    });
    test("it should parse number 4 to list", () => {
        expect(parseNumber(4)).toEqual([
            [" ", " ", " "],
            ["|", "_", "|"],
            [" ", " ", "|"]
        ])
    });
});

describe("One character number to sticks", () => {
    test("it should parse number 0 to sticks", () => {
        expect(printParsedNumber(parseNumber(0))).toEqual(" _ \n| |\n|_|\n")
    })
    test("it should parse number 1 to sticks", () => {
        expect(printParsedNumber(parseNumber(1))).toEqual("   \n  |\n  |\n")
    })
})

describe("Multiple Characters number to sticks", () => {
    test("it should parse number 10 to sticks", () => {
        expect(printListParsedNumber(parseNumbers(10))).toEqual("    _ \n  || |\n  ||_|\n")
    })
    test("it should parse number 01 to sticks", () => {
        expect(printListParsedNumber(parseNumbers("01"))).toEqual(" _    \n| |  |\n|_|  |\n")
    })
    test("it should parse number 101010 to sticks", () => {
        expect(printListParsedNumber(parseNumbers(101010))).toEqual("    _     _     _ \n  || |  || |  || |\n  ||_|  ||_|  ||_|\n")
    })
})

describe("Base Case", () => {
    test('should return nice sticks digital number 1234567890', () => {
        expect(printListParsedNumber(parseNumbers(1234567890))).toEqual("    _  _     _  _  _  _  _  _ \n  | _| _||_||_ |_   ||_||_|| |\n  ||_  _|  | _||_|  ||_| _||_|\n")
    })

})